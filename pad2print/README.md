# titre inconnu

Un outil d'édition dans le navigateur est mis en place pour l'occasion. Il utilise des pads et permet une prévisualisation en temps réel de la double-page.  
Ne fonctionne que sur Chrome ou Chromium.
![image](exemple/preview.png)

## todo
- setup: définir un format
- setup: générer une grille
- si nouvelle instance, nouveau pad documentation
- si nouvelle instance, formulaire pour nom de projet
- retirer les noms de personnes
- toggle grille
- upload images
- gestion de la double-page (si le contenu s'étale sur les deux)
- afficher setup dans l'interface
- doc passer avec une seule variable (pas une pour js et une pour php)
