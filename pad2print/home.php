<div id="cont_home">

   <?php

    $reload = $_GET['reload'];


    if($reload == 'on'){
      ?>

      <script charset="utf-8">
        setTimeout(function(){
          window.location.reload(true);
        }, 300000);
      </script>

      <?php
    }

    $home = true;

    // $setup = file_get_contents($padExport1.'setup'.$padExport2);
    // $setup = 'abcd';
    $pregEnter = array();
    $pregExit = array();
    $set_projet = array();

    // $pregEnter[0] = '/\n/';
    // $pregExit[0] = '~~';

    // $sep = preg_replace($pregEnter, $pregExit, $setup);
    $set_projets = preg_split('/~~/', $setup);
    $set_projets = array_filter($set_projets);

    $set_name = array();
    $set_slug = array();
    $set_projetName = array();

    foreach($set_projets as $set_projet){
  	 $explo = explode(' / ', $set_projet);
  	 array_push($set_name, $explo[0]);
  	 array_push($set_slug, $explo[1]);
  	 array_push($set_projetName, $explo[2]);
    }

    $i=0;
    foreach($set_slug as $slug){

      $rand = rand(0, 10000);

    	$pad_html = $padUrl.$slug.'_html';
    	$pad_css = $padUrl.$slug.'_css';
    	$pagi_odd = ($i) *2;
    	$pagi_even = $pagi_odd + 1;

      ?>

  	 <div class="double">
  	    <a class="info" href="single.php?slug=<?php echo $slug; ?>"><?php echo $slug .' <br> '. $set_name[$i] . ' <br> '. $pagi_odd .' -> '. $pagi_even; ?></a>
  	    <div id="pageGauche" class="wrap">
  	      <iframe class="pages gauche" src="pages.php?slug=<?= $slug ?>&reload=off&page=gauche<?php if($back == true) echo '&amp;bg=true'?><?php echo '&amp;rand='.$rand; ?>" allowtransparency="true"></iframe>
  	    </div>
  	    <div id="pageDroite" class="wrap">
  	      <iframe class="pages droite" src="pages.php?slug=<?= $slug ?>&reload=off&page=droite<?php if($back == true) echo '&amp;bg=true'?><?php echo '&amp;rand='.$rand; ?>" allowtransparency="true"></iframe>
  	    </div>
  	 </div>

     <?php if($back == true) { ?>
     	 <div class="doubleBack">
     	    <div id="pageGauche" class="wrap">
     	      <iframe class="pages gauche" src="back.php?slug=<?= $slug ?>&reload=off&page=gauche<?php echo '&amp;rand='.$rand; ?>"></iframe>
     	    </div>
     	    <div id="pageDroite" class="wrap">
     	      <iframe class="pages droite" src="back.php?slug=<?= $slug ?>&reload=off&page=droite<?php echo '&amp;rand='.$rand; ?>"></iframe>
     	    </div>
     	 </div>
      <?php } ?>

      <?php
    	$i++;
    }

   ?>
</div>
