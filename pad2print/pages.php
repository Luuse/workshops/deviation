  <?php
    include('include/variables.php');
    $page       = true;
    $nom        = $_GET['slug'];
    $reload     = $_GET['reload'];
    $page       = $_GET['page'];
    $bg         = $_GET['bg'];
    $pad_html   = $padUrl.$nom.'_html';
    $pad_css    = $padUrl.$nom.'_css';
    $css        = file_get_contents($pad_css.'/export/txt');
    $pageHtml   = file_get_contents($pad_html.'/export/txt');
    $search     = '/(.*?)/s';
    $result     = explode('<!-- BREAK -->', $pageHtml);
    if ($page == 'gauche'){
      $result   = $result[0];
    } else if ($page == 'droite'){
      $result   = $result[1];
    }
    include('include/header.php');
  ?>

  <?php if($bg == true){ ?>
    <svg>
      <filter id="monochrome" color-interpolation-filters="sRGB"
              x="0" y="0" height="100%" width="100%">
        <feColorMatrix type="matrix"
          values="1 0 0 0  0
                  0 0 0 0  0
                  0 0 0 0  0
                    0  0 0 1  0" />
      </filter>
    </svg>
  <?php } ?>
    <style>
      @media print, screen {
        <?php if($bg == true){ ?>
          body{
            background: none transparent;
            color: red !important;
            border-color: red !important;
            filter: url(#monochrome);
            opacity: 0.7;
            mix-blend-mode: hard-light;

          }
        <?php } ?>
        <?= $css ?>
      }
    </style>

    <?= $result ?>

  </body>
</html>
