  <!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>
			prepostprint - 1_couv		</title>

		<link rel="stylesheet" type="text/css" media="screen" href="../css/page.css"><link rel="stylesheet" type="text/css" media="screen, print" href="css/page.css">		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	</head>
  <body>

      <style>
      @media print, screen {
                @font-face {
  font-family:"manifont";
src:   url("fontes/ManifontGroteskBook.ttf");
     font-weight: normal;
     font-style: normal;
}

@font-face {
  font-family:"univers";
src:   url("fontes/UniversElse-Regular.ttf");
     font-weight: normal;
     font-style: normal;
}



h1::first-letter {
    color: red;
    }

h1{
     -webkit-hyphens: auto;
     -moz-hyphens: auto; 
     -ms-hyphens: auto;
     hyphens: auto;
    
    line-height : 0.8;
	font-size: 90pt; 
	font-family:  "univers", monospace;  
	word-wrap: break-word;
	width: 60%;
	position: absolute;
	top: 0cm;
	left: 0cm;
	margin: 0;
	padding: 30px;
	color: blue ;
	z-index: 99;
	text-transform: uppercase;
	
	}

img{
	width: 100%;
	height: auto; 
	opacity: 0.6;
}

.thumb:before{
    content:"";
    width: 21cm;
    height: 29.7cm;
    background: white;
    position: absolute;
    top: 0; 
    left:0;
    mix-blend-mode: multiply;
    }

.thumb{
    width: 20cm;
    margin-left: 0.5cm;
}

p{
	padding: 0 1cm;
}

.thumbcaption{
	padding: 0.2cm;
}

a{
    color: orange;
    text-decoration: none; 
    border: 1px solid orange;
    padding: 0cm 0.1cm;
}

#circles{
    position: absolute;
    left:0;
    bottom:0;
    width: 110vw;
    height:60px;
}

.circle{
    display:inline-block;
  bottom:0px;
    left: 0px;
width: 0;        
 height: 0;         
 border-left: 80px solid transparent;         
 border-right: 80px solid transparent;         
 border-bottom: 100px solid gold;
     z-index: 100;
}


    
    
    
    


      }
    </style>

    
 <h1 class="firstHeading" id="firstHeading" lang="en"><em>Mona Lisa</em> replicas and reinterpretations</h1>


        <div class="mw-body-content" id="bodyContent">
                <div id="siteSub">
                        From Wikipedia, the free encyclopedia
                </div>


                <div class="mw-content-ltr" dir="ltr" id="mw-content-text" lang="en">
                        <div class="thumb tright">
                                <div class="thumbinner">
                                        <a class="image" href="/wiki/File:Sapeck-La_Joconde_fumant_la_pipe.jpg"><img alt="" class="thumbimage" data-file-height="1280" data-file-width="853" height="300" src="//upload.wikimedia.org/wikipedia/commons/thumb/1/16/Sapeck-La_Joconde_fumant_la_pipe.jpg/200px-Sapeck-La_Joconde_fumant_la_pipe.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/1/16/Sapeck-La_Joconde_fumant_la_pipe.jpg/300px-Sapeck-La_Joconde_fumant_la_pipe.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/1/16/Sapeck-La_Joconde_fumant_la_pipe.jpg/400px-Sapeck-La_Joconde_fumant_la_pipe.jpg 2x" width="200"></a>

                                        <div class="thumbcaption">
                                                <em>Le rire</em> (The Laugh) by Sapeck (Eug&egrave;ne Bataille), 1883
                                        </div>
                                </div>
                        </div>


                        <div class="thumb tmulti tright">
                                <div class="thumbinner">
                                        <div class="tsingle">
                                                <div class="thumbimage">
                                                        <a class="image" href="/wiki/File:Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg"><img alt="" data-file-height="636" data-file-width="800" height="207" src="//upload.wikimedia.org/wikipedia/commons/thumb/1/19/Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg/260px-Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/1/19/Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg/390px-Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/1/19/Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg/520px-Louis_Beroud_-_Mona_Lisa_au_Louvre_1911.jpg 2x" width="260"></a>
                                                </div>
                                        </div>


                                        <div class="tsingle">
                                                <div class="thumbimage">
                                                        <a class="image" href="/wiki/File:Mona_Lisa_stolen-1911.jpg"><img alt="" data-file-height="287" data-file-width="308" height="207" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Mona_Lisa_stolen-1911.jpg/222px-Mona_Lisa_stolen-1911.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/f/f8/Mona_Lisa_stolen-1911.jpg 1.5x" width="222"></a>
                                                </div>
                                        </div>


                                        <div class="thumbcaption">
                                                A <em>painting-of-the-painting</em> (left) depicting <em><a href="/wiki/Mona_Lisa" title="Mona Lisa">Mona Lisa</a></em> as displayed in <a class="mw-redirect" href="/wiki/The_Louvre" title="The Louvre">the Louvre</a> (<a href="/wiki/Louis_B%C3%A9roud" title="Louis B&eacute;roud">Louis B&eacute;roud</a>, 1911). At right, the same wall shown in a photograph taken after the painting's theft, also 1911.
                                        </div>
                                </div>
                        </div>


                        <p><a href="/wiki/Leonardo_da_Vinci" title="Leonardo da Vinci">Leonardo da Vinci</a>'s <em><a href="/wiki/Mona_Lisa" title="Mona Lisa">Mona Lisa</a></em> is one of the most recognizable and famous works of art in the world, and also one of the most replicated and reinterpreted. <strong><em>Mona Lisa</em> replicas</strong> were already being painted during Leonardo's lifetime by his own students and contemporaries. Some are claimed to be the work of Leonardo himself, and remain disputed by scholars. Prominent 20th-century artists such as <a href="/wiki/Marcel_Duchamp" title="Marcel Duchamp">Marcel Duchamp</a> and <a href="/wiki/Salvador_Dal%C3%AD" title="Salvador Dal&iacute;">Salvador Dal&iacute;</a> have also produced derivative works, manipulating <em>Mona Lisa'</em>s image to suit their own aesthetic. Replicating <a href="/wiki/Renaissance" title="Renaissance">Renaissance</a> masterpieces continues to be a way for aspiring artists to perfect their painting techniques and prove their skills.<sup class="reference" id="cite_ref-Cottage_Industry_1-0"><a href="#cite_note-Cottage_Industry-1">[1]</a></sup></p>


                        <p>Contemporary <em>Mona Lisa</em> replicas are often created in conjunction with events or exhibitions related to Leonardo da Vinci, for publicity. When scientists did a scan on each layer of paint they found that Mona Lisa included many people all completely different before Mona Lisa was finished. Also, archeologists found a skull they believe belongs to Mona Lisa. Her <a href="/wiki/Portrait" title="Portrait">portrait</a>, considered <a href="/wiki/Public_domain" title="Public domain">public domain</a> and therefore outside of <a href="/wiki/Copyright" title="Copyright">copyright</a> protection, has also been exploited to make <a href="/wiki/Political_statement" title="Political statement">political statements</a>. Known even to people with no art background, the mere use of <em>Mona Lisa'</em>s name&nbsp;&ndash; immortalized in </p>


                                                                                                              
                                                                                                    
                                                                                                              


  </body>
</html>
