  <!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>
			prepostprint - content2		</title>

		<link rel="stylesheet" type="text/css" media="screen" href="../css/page.css"><link rel="stylesheet" type="text/css" media="screen, print" href="css/page.css">		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	</head>
  <body>

      <style>
      @media print, screen {
                h2{
	width: 4cm; 
	transform: rotate(10deg);
	margin-top: 1cm;
	margin-left: 1cm;
	color: coral;
}

h3{
  color: blue; 
  margin-left: 8cm;  
}

.thumbcaption{
    width: 4cm;
    position: relative;
    top: 8cm;
    left: 1cm;
    transform: rotate(5deg);

    }

img{
    position: absolute;
    transform: rotate(-10deg);
    }
    
p{
	width: 14cm;
	margin-left: 6cm;  
	background-color: coral;  
}

body {
    background-color: aqua;
    font-family:"Comic Sans MS", cursive, sans-serif;

    }
 
 img {
  border-radius: 50%;
  }   
  
img:hover {
    border-radius: 10%;
*}
 
    
    
    
      }
    </style>

    
                        <h2><span class="mw-headline" id="Mona_Vanna"><em>Mona Vanna VANITAS, VANIDADES</em></span>
                        </h2>


                        <p>Two NUDES! paintings bearing similarities to Leonardo da Vinci's original were part of a 2009 exhibition of artwork inspired by <em>Mona Lisa</em>. Displayed at the Museo Ideale in Leonardo's hometown of <a href="/wiki/Vinci,_Tuscany" title="Vinci, Tuscany">Vinci</a>, near <a href="/wiki/Florence" title="Florence">Florence</a>, some believe one of the paintings&nbsp;&ndash; dating from Leonardo's time<sup class="reference" id="cite_ref-NUDE_mona_2_7-1"><a href="#cite_note-NUDE_mona_2-7">[7]</a></sup>&nbsp;&ndash; to be the work of Leonardo himself, and it has at times been credited to him.<sup class="reference" id="cite_ref-NUDE_mona_14-0"><a href="#cite_note-NUDE_mona-14">[14]</a></sup> Other experts theorize the painting, one of at least six known to exist, may be just another copy painted by "followers" of Leonardo. Scholarly dispute persists as to artist, subject and origin.<sup class="reference" id="cite_ref-Daily_News_news_15-0"><a href="#cite_note-Daily_News_news-15">[15]</a></sup> The nude in question, discovered behind a wall in a private library, reportedly belonged to an uncle of <a class="mw-redirect" href="/wiki/Napoleon_Bonaparte" title="Napoleon Bonaparte">Napoleon Bonaparte</a>, who owned another of Leonardo's paintings.<sup class="reference" id="cite_ref-NUDE_mona_2_7-2"><a href="#cite_note-NUDE_mona_2-7">[7]</a></sup> Facial features bear only vague resemblance, but landscape, compositional and technical details correspond to those of the <em>Mona Lisa</em> known worldwide today.<sup class="reference" id="cite_ref-NUDE_mona_14-1"><a href="#cite_note-NUDE_mona-14">[14]</a></sup><sup class="reference" id="cite_ref-Daily_News_news_15-1"><a href="#cite_note-Daily_News_news-15">[15]</a></sup></p>


                        <div class="thumb tleft">
                                <div class="thumbinner">
                                        <a class="image" href="/wiki/File:Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg"><img alt="" class="thumbimage" data-file-height="743" data-file-width="579" height="282" src="//upload.wikimedia.org/wikipedia/commons/thumb/6/69/Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg/220px-Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/6/69/Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg/330px-Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/6/69/Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg/440px-Mona_Vanna_Nuda_by_Joos_van_Cleve.jpg 2x" width="220"></a>

                                        <div class="thumbcaption">
                                                <a href="/wiki/Joos_van_Cleve" title="Joos van Cleve">Joos van Cleve</a>'s <em>Mona Vanna</em>
                                        </div>
                                </div>
                        </div>


                        <p>A student and companion of Leonardo da Vinci known as <a href="/wiki/Sala%C3%AC" title="Sala&igrave;">Sala&igrave;</a> painted one of the nude interpretations of <em>Mona Lisa</em> known, titled <em>Mona Vanna</em>. Salai's version is thought by some to have been "based on" the nude sometimes attributed to Leonardo, which is considered a <a href="/wiki/Lost_work" title="Lost work">lost work</a>. Discussion among experts exists as to whether Salai, known to have modeled for Leonardo, may in fact have been the sitter represented in the original <em>Mona Lisa</em>.<sup class="reference" id="cite_ref-male_mona_16-0"><a href="#cite_note-male_mona-16">[16]</a></sup><sup class="reference" id="cite_ref-LdV_lost_nude_17-0"><a href="#cite_note-LdV_lost_nude-17">[17]</a></sup></p>


                        <p><a href="/wiki/Joos_van_Cleve" title="Joos van Cleve">Joos van Cleve</a>, a Flemish artist active in the years following <em>Mona Lisa'</em>s creation, also painted a nude titled <em>Mona Vanna</em>. Though the figure portrayed in van Cleve's painting bears no resemblance to Leonardo's <em>Mona Lisa</em>, the artist was known to mimic themes and techniques of Leonardo da Vinci, in this case the positioning of the figure and the delicate brushwork reminiscent of Leonardo's <a href="/wiki/Sfumato" title="Sfumato">sfumato</a>. The artwork, dating to the mid-16th century, is in the collection of the <a href="/wiki/National_Gallery_in_Prague" title="National Gallery in Prague">National Gallery</a>, <a href="/wiki/Prague" title="Prague">Prague</a>.<sup class="reference" id="cite_ref-Joos_18-0"><a href="#cite_note-Joos-18">[18]</a></sup></p>


                        <h2><span class="mw-headline" id="20th_century">20th century</span>
                        </h2>


                        <p>By the 20th century, <em>Mona Lisa</em> had already been a victim of satirical embellishment. <em>Sapeck</em> (Eug&egrave;ne Bataille), in 1883, depicted <em>Mona Lisa</em> smoking a pipe (<em>shown above</em>). Titled <em>Le Rire</em> (The Laugh), the artwork was displayed at the "<a href="/wiki/Incoherents" title="Incoherents">Incoherents</a>" exhibition in Paris at the time of its creation, making it among the earliest known instances of <em>Mona Lisa'</em>s image being re-interpreted using contemporary <a href="/wiki/Irony" title="Irony">irony</a>. Further interpretations by <a class="mw-redirect" href="/wiki/Avante_garde" title="Avante garde">avante garde</a> artists beginning in the early 20th century, coinciding with the artwork's theft, attest to <em>Mona Lisa'</em>s popularity as an irresistible target. <a href="/wiki/Dada" title="Dada">Dadaists</a> and <a href="/wiki/Surrealism" title="Surrealism">Surrealists</a> were quick to modify, embellish and <a href="/wiki/Caricature" title="Caricature">caricature</a><em>Mona Lisa'</em>s visage.</p>


                        <div class="thumb tmulti tright">
                                <div class="thumbinner">
                                        <div class="tsingle">
                                                <div class="thumbimage">
                                                        <a class="image" href="/wiki/File:Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg"><img alt="" data-file-height="768" data-file-width="552" height="269" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg/193px-Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg/290px-Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg/386px-Brooklyn_Museum_-_Mona_Lisa_-_Timothy_Cole.jpg 2x" width="193"></a>
                                                </div>
                                        </div>


  </body>
</html>
